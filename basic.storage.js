/* BASIC BROWSER STORAGE VS 0.5*/
basic.extend({
    storage: window.localStorage,
    store: function(){
        if(arguments.length < 1)return this;
        var options = arguments[0] && typeof arguments[0] == "object" &&  typeof arguments[0] != undefined? arguments[0] : null, value;
        if(options != null){
            basic.extend(basic.storage, options);
        }else{
            if(typeof arguments[0] == "string"){
                value = arguments[1] && typeof arguments[1] != null? arguments[1] : null;
                basic.storage[arguments[0]] = value;
            }
        } 
    }
});