/* BASIC ELEMENT MANIPULATION VS 0.51 */
basic.extend({
    replaceClass: function (oldClass, newClass) {
        if(typeof oldClass !== "string" && typeof newClass !== "string")return this;
        
        this.removeClass(oldClass);
        this.addClass(newClass);
        return this;
    },
    removeClass: function (cName) {
        this.classList.remove(cName);
        return this
    },
    addClass: function (cName) {
        this.classList.add(cName);
        return this;
    },
    toggleClass: function (cName) {
        var cName = cName && typeof cName == "string" ? cName : null,
            relElem = arguments[1] && typeof arguments[1] == "object" ? arguments[1] : null,
            cb = arguments[1] && typeof arguments[1] == "function" ? arguments[1] : null;
        if (cName) {
            if (this.classList && basic.inArray(this.classList, cName)) this.removeClass(cName)
            else this.addClass(cName);

            if (relElem) relElem.toggleClass(cName);
            if (cb) cb.call(this);
        }
        return this;
    },
    attr:function(name, value) {
        name = name? name : null,
        value = value?value : null;
        if (value) {
            object.setAttribute(name, value);
            return this;	
        } else {
            return this.getAttribute(name);	
        }
	},
    autoResize : function () {//works on textarea fields only right now
        var self = this;
        if(self.getAttribute("default-rows") && this.tagName == "textarea"){
            self.setAttribute("default-rows", self.rows);
            self.onkeyup = function() {
                var l = Math.round(this.value.length/this.cols);
                this.rows = l > this.rows ? l : this.rows;
                this.rows = l < this.getAttribute("default-rows")? this.getAttribute("default-rows") : this.rows;
            };
        }
    }
});