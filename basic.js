var basicVersion = 0.51,
basicArray = [],
basicObjLit = {},
basicSlice = basicArray.slice,
basicPush = basicArray.push,
basicHasOwn = basicObjLit.hasOwnProperty,
basicToString = basicObjLit.toString,

$ = basic = function(obj) {
    return new basic.prototype.init(obj);
};

basic.prototype = {
    version: basicVersion,
    init: function() {
        var obj = arguments[0] && typeof arguments[0] == "object" ? arguments[0] : null,
            selector =  arguments[0] && typeof arguments[0] == "string" && obj == null? arguments[0] : null;
                
        if(obj){
            return basic.extend(obj, basic);
        }else if(selector){
            elems = document.querySelectorAll(selector);
            for(I in elems){
                if(elems.hasOwnProperty(I)){
                    elems[I] = basic.extend(elems[I], basic);
                }
            }
            return basic.extend(elems.length > 1? basicArray.slice.call(elems) : elems[0], basic);
        }
    }
};

basic.extend = basic.prototype.extend = function () {
    var options, name, src, copy, copyIsArray, clone,
        target = arguments[0] || {},
        i = 1,
        length = arguments.length,
        deep = false;

    if (typeof target === "boolean") {
        deep = target;
        target = arguments[1] || {};
        i = 2;
    }
    target = (typeof target !== "object" && typeof target !== "function") ? {} : target;
    
    if (length === i) {
        target = this;
        --i;
    }
    
    for (; i < length; i++) {
        if ((options = arguments[i]) != null) {
            // Extend the basic object
            for (name in options) {
                src = target[name];
                copy = options[name];
                //console.log("copy:", name);
                // Prevent never-ending loop or replicating an already built method
                if (target === copy || target[name]) {
                    continue;
                }
                //console.log("target");
                // Recurse if we're merging plain objects or arrays
                if (deep && copy && (copyIsArray = basic.isArray(copy))) {
                    if (copyIsArray) {
                        copyIsArray = false;
                        clone = src && basic.isArray(src) ? src : [];
                    } else {
                        clone = src && basic.isPlainObject(src)? src : {};
                    }
                    // Never move original objects, clone them
                    target[name] = basic.extend(deep, clone, copy);
                } else if (copy !== undefined) {
                    target[name] = copy;
                }
                /*if(basic.overloadArrayPrototype){
                    basic.overloadArrayPrototype(name);
                }*/
            }
        }
    }
    return target;
};


/* BALLS BASE */
basic.extend({
    isArray: Array.isArray,
    isArraylike: function (obj) {
        var length = obj.length,
            type = this.type(obj);
        if (this.isWindow(obj)) { return false; }
        if (obj.nodeType === 1 && length) {  return true; }
    
        return type === "array" || type !== "function" && (length === 0 || typeof length === "number" && length > 0 && (length - 1) in obj);
    },
    isWindow: function (obj) { return obj != null && obj === obj.window; },
    isNumeric: function (obj) { return !isNaN(parseFloat(obj)) && isFinite(obj); },
    inArray: function (value, array){
        var i;
        for (i=0; i < array.length; i++){
            if (array[i] == value){return true;}
        }
        return false;
    },
    toArray: function () { return basicSlice.call(this); },
    toJson: JSON.parse,
    toTimeFormat: function (seconds) {
        var sec_num = parseInt(seconds, 10), // don't forget the second param
            hours   = Math.floor(sec_num / 3600),
            minutes = Math.floor((sec_num - (hours * 3600)) / 60),
            seconds = sec_num - (hours * 3600) - (minutes * 60);
        if (hours   < 10) {hours  = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}

        return (hours != "00")? hours+':'+minutes+':'+seconds : minutes+':'+seconds;
    },
    
    type: function (obj) {
        if (obj == null) { return String(obj); }
        return typeof obj === "object" || typeof obj === "function" ? basicObjLit[basicToString.call(obj)] || "object" : typeof obj;
    },
    isPlainObject: function (obj) {
        if (basic.type(obj) !== "object" || obj.nodeType || basic.isWindow(obj)) { return false; }
        try {
            if (obj.constructor && !basicHasOwn.call(obj.constructor.prototype, "isPrototypeOf")) { return false; }
        } catch (e) { return false;  }
        return true;
    },
    each:function() {
        var object = arguments[0]?arguments[0]:null,
            callback = arguemnts[1] && typeof arguments[1] == "function"? arguments[1] : function(){};
		if(object){
            for (var i = 0; i < object.length; i++) {
                callback(object[i], i);
            }
        }
        return this;
    }/*,
    overloadArrayPrototype: function () {
        var x = arguments[0]? arguments[0] : null;
        console.log(x);
        if(typeof Array[x] != "function" && x != null){
            Array.prototype[x] = function () {
                for(I in this){
                    if(this.hasOwnProperty(I)){
                        basic[x].apply(this[I], arguments);
                    }
                }
                return this;
            }
        }
        return this;
    }*/
});