/* BASIC BROWSER EVENTS VS 0.5 */
basic.extend({
    on:function () {
        //Not Enough Arguments
        if(arguments.length < 2) return this;
        //arguments[0] = event
        //arguments[1] = callBack
        if(arguments.length == 2 && typeof arguments[1] == "function"){
            if(basic.isArraylike(this)){
                for(I in this){
                    if (this[I].addEventListener) {
                        this[I].addEventListener(arguments[0], arguments[1], false);
                    } else if (this[I].attachEvent){
                        this[I].attachEvent("on"+arguments[0], arguments[1]);
                    }
                }
            }else{
                if (this.addEventListener){
                    this.addEventListener(arguments[0], arguments[1], false);
                } else {
                    this.attachEvent("on"+arguments[0], arguments[1]);
                }
            }
        }
        //arguments[0] = event
        //arguments[1] = delegate
        //arguments[2] = callBack
        if(arguments.length == 3 && typeof arguments[2] == "function"){
            var cb = arguments[2];
            if(typeof arguments[1] == "string"){
                var delegates = document.querySelectorAll(arguments[1]), I;
                //console.log("is array:", basic.isArraylike(this));
                if(basic.isArraylike(this)){
                    for(I in this){
                        if(this.hasOwnProperty[I] && basic.isPlainObject(this[I])){
                            this[I].on(arguments[0], function(e){
                                delegates = this[I].querySelectorAll(delegates);
                                //if target is in delegates
                                if(basic.inArray(e.target, delegates)){
                                    cb[2](e);
                                }
                            });
                        }
                    }
                }else{
                    this.on(arguments[0], function(e){
                        if(basic.inArray(e.target, delegates)){
                            cb(e);
                        }
                    });
                }
            }
        }
        return this;
    }
});
