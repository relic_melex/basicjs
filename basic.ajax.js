/* BASIC AJAX VS 0.5*/
basic.extend({
    queue:function () {
        //append to queues.active
        // if queue'd action alreay happeneing stop or override based on incomming vars
    },
    queues:{
        init: function (defaults) {
            defaults = defaults && basic.isPlainObject(defaults)? defaults : null;
            if(defaults === null) return false;
            
            for(I in defaults){
                if(defaults.hasOwnProperty(I)){
                    basic.queues.active[I] =  defaults[I];
                }
            }
        },
        cleanup: function () {
            //dump active session with arguments[0] as an id
        },
        active: {}//list of active queues for ajax requests
    },
    ajax: function (params) {
        var xmlhttp, self = this, url = "", httpdata = [], header= "application/x-www-form-urlencoded", method = "get",
            params = params && basic.isPlainObject(params) ? params : null, param;
        
        if (params === null) return this;
        
        for (param in params) {
            switch (param) {
                case 'url': url = params[param];  break;
                case 'data': httpdata = params[param];  break;
                case 'header': header = params[param];  break;
                case 'type': method = params[param]; break;
            }
        }
        if (url == "") return this;

        xmlhttp = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        xmlhttp.parent = self;
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if (this.parent.onfailure && typeof this.parent.onfailure == "function" && (xmlhttp.responseText.indexOf("Denied") > -1 || xmlhttp.responseText.indexOf("Error") > -1 || xmlhttp.responseText.indexOf("Fail") > -1)) this.parent.onfailure(xmlhttp.responseText);
                else if (this.parent.onsuccess && typeof this.parent.onsuccess == "function") this.parent.onsuccess(xmlhttp.responseText);
                if (this.parent.onalways && typeof this.parent.onalways == "function"){
                    console.log("always");
                    this.parent.onalways(xmlhttp.responseText);
                }
            } else if ((xmlhttp.readyState == 4 || xmlhttp.readyState == 3) && xmlhttp.status < 200) {
                if (this.parent.onfailure && typeof this.parent.onfailure == "function"){
                    this.parent.onfailure(xmlhttp.description);
                }
                if (this.parent.onalways && typeof this.parent.onalways == "function"){
                    this.parent.onalways(xmlhttp.responseText);
                }
            }
            this.parent.onscuccess = null;
            this.parent.onfailure = null;
            this.parent.onalways = null;
        }
        tempparams = "";
        for (var I in httpdata) {
            tempparams += (tempparams.length > 0) ? "&" + I + "=" + httpdata[I] : I + "=" + httpdata[I];
        }
        xmlhttp.open(method, url, true);
        xmlhttp.setRequestHeader("Content-type",header);
        if (typeof params == 'object') {
            xmlhttp.send(tempparams);
        }
        return this;
    },
    done: function (cb) {
        cb = cb && typeof cb == "function" ? cb : null;
        if (cb == null) return this;
        this.onsuccess = cb;
        return this;
    },
    failure: function (cb) {
        cb = cb && typeof cb == "function" ? cb : null;
        if (cb == null) return this;
        this.onfailure = cb;
        return this;
    },
    always: function (cb) {
        cb = cb && typeof cb == "function" ? cb : null;
        if (cb == null) return this;
        this.onalways = cb;
        return this;
    }
});
